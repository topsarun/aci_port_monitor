﻿namespace ACI_PORT_MONITOR
{
    partial class MAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Status_Strip_1 = new System.Windows.Forms.StatusStrip();
            this.Status_bar = new System.Windows.Forms.ToolStripStatusLabel();
            this.Node201_Box = new System.Windows.Forms.GroupBox();
            this.Health_201_Score = new System.Windows.Forms.Label();
            this.Node202_Box = new System.Windows.Forms.GroupBox();
            this.Health_202_Score = new System.Windows.Forms.Label();
            this.Node401_Box = new System.Windows.Forms.GroupBox();
            this.Health_401_Score = new System.Windows.Forms.Label();
            this.Node402_Box = new System.Windows.Forms.GroupBox();
            this.Health_402_Score = new System.Windows.Forms.Label();
            this.Node203_Box = new System.Windows.Forms.GroupBox();
            this.Health_203_Score = new System.Windows.Forms.Label();
            this.Node204_Box = new System.Windows.Forms.GroupBox();
            this.Health_204_Score = new System.Windows.Forms.Label();
            this.Node403_Box = new System.Windows.Forms.GroupBox();
            this.Health_403_Score = new System.Windows.Forms.Label();
            this.Node404_Box = new System.Windows.Forms.GroupBox();
            this.Health_404_Score = new System.Windows.Forms.Label();
            this.Domain1_Box = new System.Windows.Forms.GroupBox();
            this.Domain2_Box = new System.Windows.Forms.GroupBox();
            this.Node612_Box = new System.Windows.Forms.GroupBox();
            this.Health_612_Score = new System.Windows.Forms.Label();
            this.Node611_Box = new System.Windows.Forms.GroupBox();
            this.Health_611_Score = new System.Windows.Forms.Label();
            this.Node211_Box = new System.Windows.Forms.GroupBox();
            this.Health_211_Score = new System.Windows.Forms.Label();
            this.Node212_Box = new System.Windows.Forms.GroupBox();
            this.Health_212_Score = new System.Windows.Forms.Label();
            this.Node412_Box = new System.Windows.Forms.GroupBox();
            this.Health_412_Score = new System.Windows.Forms.Label();
            this.Node411_Box = new System.Windows.Forms.GroupBox();
            this.Health_411_Score = new System.Windows.Forms.Label();
            this.Domain3_Box = new System.Windows.Forms.GroupBox();
            this.Node622_Box = new System.Windows.Forms.GroupBox();
            this.Health_622_Score = new System.Windows.Forms.Label();
            this.Node621_Box = new System.Windows.Forms.GroupBox();
            this.Health_621_Score = new System.Windows.Forms.Label();
            this.Node422_Box = new System.Windows.Forms.GroupBox();
            this.Health_422_Score = new System.Windows.Forms.Label();
            this.Node421_Box = new System.Windows.Forms.GroupBox();
            this.Health_421_Score = new System.Windows.Forms.Label();
            this.Menu_Strip_1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dEBUGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Fabic_Box = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Health_632_Score = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Health_631_Score = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Health_232_Score = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Health_231_Score = new System.Windows.Forms.Label();
            this.Health_POD1_score = new System.Windows.Forms.TextBox();
            this.Node502_Box = new System.Windows.Forms.GroupBox();
            this.Health_502_Score = new System.Windows.Forms.Label();
            this.Node501_Box = new System.Windows.Forms.GroupBox();
            this.Health_501_Score = new System.Windows.Forms.Label();
            this.Node302_Box = new System.Windows.Forms.GroupBox();
            this.Health_302_Score = new System.Windows.Forms.Label();
            this.Node301_Box = new System.Windows.Forms.GroupBox();
            this.Health_301_Score = new System.Windows.Forms.Label();
            this.Node102_Box = new System.Windows.Forms.GroupBox();
            this.Health_102_Score = new System.Windows.Forms.Label();
            this.Node101_Box = new System.Windows.Forms.GroupBox();
            this.Health_101_Score = new System.Windows.Forms.Label();
            this.AAA_Refresh = new System.Timers.Timer();
            this.BGW_POD1 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Spine_101 = new System.ComponentModel.BackgroundWorker();
            this.Timer_SPINE_BGW = new System.Windows.Forms.Timer(this.components);
            this.BGW_Spine_102 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Spine_301 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Spine_302 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Spine_501 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Spine_502 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_201 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_202 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_203 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_204 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_401 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_402 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_403 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_404 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_211 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_212 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_411 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_412 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_611 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_612 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_221 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_222 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_421 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_422 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_621 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_622 = new System.ComponentModel.BackgroundWorker();
            this.Timer_DOM1_BGW = new System.Windows.Forms.Timer(this.components);
            this.Timer_DOM2_BGW = new System.Windows.Forms.Timer(this.components);
            this.Timer_DOM3_BGW = new System.Windows.Forms.Timer(this.components);
            this.Info_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.BGW_Node_231 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_232 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_631 = new System.ComponentModel.BackgroundWorker();
            this.BGW_Node_632 = new System.ComponentModel.BackgroundWorker();
            this.Timer_DOM4_BGW = new System.Windows.Forms.Timer(this.components);
            this.Status_Strip_1.SuspendLayout();
            this.Node201_Box.SuspendLayout();
            this.Node202_Box.SuspendLayout();
            this.Node401_Box.SuspendLayout();
            this.Node402_Box.SuspendLayout();
            this.Node203_Box.SuspendLayout();
            this.Node204_Box.SuspendLayout();
            this.Node403_Box.SuspendLayout();
            this.Node404_Box.SuspendLayout();
            this.Domain1_Box.SuspendLayout();
            this.Domain2_Box.SuspendLayout();
            this.Node612_Box.SuspendLayout();
            this.Node611_Box.SuspendLayout();
            this.Node211_Box.SuspendLayout();
            this.Node212_Box.SuspendLayout();
            this.Node412_Box.SuspendLayout();
            this.Node411_Box.SuspendLayout();
            this.Domain3_Box.SuspendLayout();
            this.Node622_Box.SuspendLayout();
            this.Node621_Box.SuspendLayout();
            this.Node422_Box.SuspendLayout();
            this.Node421_Box.SuspendLayout();
            this.Menu_Strip_1.SuspendLayout();
            this.Fabic_Box.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Node502_Box.SuspendLayout();
            this.Node501_Box.SuspendLayout();
            this.Node302_Box.SuspendLayout();
            this.Node301_Box.SuspendLayout();
            this.Node102_Box.SuspendLayout();
            this.Node101_Box.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AAA_Refresh)).BeginInit();
            this.SuspendLayout();
            // 
            // Status_Strip_1
            // 
            this.Status_Strip_1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status_bar});
            this.Status_Strip_1.Location = new System.Drawing.Point(0, 518);
            this.Status_Strip_1.Name = "Status_Strip_1";
            this.Status_Strip_1.Size = new System.Drawing.Size(960, 22);
            this.Status_Strip_1.SizingGrip = false;
            this.Status_Strip_1.TabIndex = 0;
            this.Status_Strip_1.Text = "statusStrip1";
            // 
            // Status_bar
            // 
            this.Status_bar.Name = "Status_bar";
            this.Status_bar.Size = new System.Drawing.Size(39, 17);
            this.Status_bar.Text = "Status";
            // 
            // Node201_Box
            // 
            this.Node201_Box.Controls.Add(this.Health_201_Score);
            this.Node201_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node201_Box.Location = new System.Drawing.Point(6, 37);
            this.Node201_Box.Name = "Node201_Box";
            this.Node201_Box.Size = new System.Drawing.Size(103, 76);
            this.Node201_Box.TabIndex = 2;
            this.Node201_Box.TabStop = false;
            this.Node201_Box.Text = "NODE 201";
            // 
            // Health_201_Score
            // 
            this.Health_201_Score.AutoSize = true;
            this.Health_201_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_201_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_201_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_201_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_201_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_201_Score.Name = "Health_201_Score";
            this.Health_201_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_201_Score.TabIndex = 0;
            this.Health_201_Score.Text = "N/A";
            this.Health_201_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_201_Score.TextChanged += new System.EventHandler(this.Health_201_Score_TextChanged);
            // 
            // Node202_Box
            // 
            this.Node202_Box.Controls.Add(this.Health_202_Score);
            this.Node202_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node202_Box.Location = new System.Drawing.Point(6, 119);
            this.Node202_Box.Name = "Node202_Box";
            this.Node202_Box.Size = new System.Drawing.Size(103, 76);
            this.Node202_Box.TabIndex = 3;
            this.Node202_Box.TabStop = false;
            this.Node202_Box.Text = "NODE 202";
            // 
            // Health_202_Score
            // 
            this.Health_202_Score.AutoSize = true;
            this.Health_202_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_202_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_202_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_202_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_202_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_202_Score.Name = "Health_202_Score";
            this.Health_202_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_202_Score.TabIndex = 1;
            this.Health_202_Score.Text = "N/A";
            this.Health_202_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_202_Score.TextChanged += new System.EventHandler(this.Health_202_Score_TextChanged);
            // 
            // Node401_Box
            // 
            this.Node401_Box.Controls.Add(this.Health_401_Score);
            this.Node401_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node401_Box.Location = new System.Drawing.Point(224, 37);
            this.Node401_Box.Name = "Node401_Box";
            this.Node401_Box.Size = new System.Drawing.Size(103, 76);
            this.Node401_Box.TabIndex = 4;
            this.Node401_Box.TabStop = false;
            this.Node401_Box.Text = "NODE 401";
            // 
            // Health_401_Score
            // 
            this.Health_401_Score.AutoSize = true;
            this.Health_401_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_401_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_401_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_401_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_401_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_401_Score.Name = "Health_401_Score";
            this.Health_401_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_401_Score.TabIndex = 4;
            this.Health_401_Score.Text = "N/A";
            this.Health_401_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_401_Score.TextChanged += new System.EventHandler(this.Health_401_Score_TextChanged);
            // 
            // Node402_Box
            // 
            this.Node402_Box.Controls.Add(this.Health_402_Score);
            this.Node402_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node402_Box.Location = new System.Drawing.Point(333, 37);
            this.Node402_Box.Name = "Node402_Box";
            this.Node402_Box.Size = new System.Drawing.Size(103, 76);
            this.Node402_Box.TabIndex = 5;
            this.Node402_Box.TabStop = false;
            this.Node402_Box.Text = "NODE 402";
            // 
            // Health_402_Score
            // 
            this.Health_402_Score.AutoSize = true;
            this.Health_402_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_402_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_402_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_402_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_402_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_402_Score.Name = "Health_402_Score";
            this.Health_402_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_402_Score.TabIndex = 5;
            this.Health_402_Score.Text = "N/A";
            this.Health_402_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_402_Score.TextChanged += new System.EventHandler(this.Health_402_Score_TextChanged);
            // 
            // Node203_Box
            // 
            this.Node203_Box.Controls.Add(this.Health_203_Score);
            this.Node203_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node203_Box.Location = new System.Drawing.Point(115, 37);
            this.Node203_Box.Name = "Node203_Box";
            this.Node203_Box.Size = new System.Drawing.Size(103, 76);
            this.Node203_Box.TabIndex = 4;
            this.Node203_Box.TabStop = false;
            this.Node203_Box.Text = "NODE 203";
            // 
            // Health_203_Score
            // 
            this.Health_203_Score.AutoSize = true;
            this.Health_203_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_203_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_203_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_203_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_203_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_203_Score.Name = "Health_203_Score";
            this.Health_203_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_203_Score.TabIndex = 2;
            this.Health_203_Score.Text = "N/A";
            this.Health_203_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_203_Score.TextChanged += new System.EventHandler(this.Health_203_Score_TextChanged);
            // 
            // Node204_Box
            // 
            this.Node204_Box.Controls.Add(this.Health_204_Score);
            this.Node204_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node204_Box.Location = new System.Drawing.Point(115, 119);
            this.Node204_Box.Name = "Node204_Box";
            this.Node204_Box.Size = new System.Drawing.Size(103, 76);
            this.Node204_Box.TabIndex = 8;
            this.Node204_Box.TabStop = false;
            this.Node204_Box.Text = "NODE 204";
            // 
            // Health_204_Score
            // 
            this.Health_204_Score.AutoSize = true;
            this.Health_204_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_204_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_204_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_204_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_204_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_204_Score.Name = "Health_204_Score";
            this.Health_204_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_204_Score.TabIndex = 3;
            this.Health_204_Score.Text = "N/A";
            this.Health_204_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_204_Score.TextChanged += new System.EventHandler(this.Health_204_Score_TextChanged);
            // 
            // Node403_Box
            // 
            this.Node403_Box.Controls.Add(this.Health_403_Score);
            this.Node403_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node403_Box.Location = new System.Drawing.Point(224, 119);
            this.Node403_Box.Name = "Node403_Box";
            this.Node403_Box.Size = new System.Drawing.Size(103, 76);
            this.Node403_Box.TabIndex = 9;
            this.Node403_Box.TabStop = false;
            this.Node403_Box.Text = "NODE 403";
            // 
            // Health_403_Score
            // 
            this.Health_403_Score.AutoSize = true;
            this.Health_403_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_403_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_403_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_403_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_403_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_403_Score.Name = "Health_403_Score";
            this.Health_403_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_403_Score.TabIndex = 6;
            this.Health_403_Score.Text = "N/A";
            this.Health_403_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_403_Score.TextChanged += new System.EventHandler(this.Health_403_Score_TextChanged);
            // 
            // Node404_Box
            // 
            this.Node404_Box.Controls.Add(this.Health_404_Score);
            this.Node404_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node404_Box.Location = new System.Drawing.Point(333, 119);
            this.Node404_Box.Name = "Node404_Box";
            this.Node404_Box.Size = new System.Drawing.Size(103, 76);
            this.Node404_Box.TabIndex = 10;
            this.Node404_Box.TabStop = false;
            this.Node404_Box.Text = "NODE 404";
            // 
            // Health_404_Score
            // 
            this.Health_404_Score.AutoSize = true;
            this.Health_404_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_404_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_404_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_404_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_404_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_404_Score.Name = "Health_404_Score";
            this.Health_404_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_404_Score.TabIndex = 7;
            this.Health_404_Score.Text = "N/A";
            this.Health_404_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_404_Score.TextChanged += new System.EventHandler(this.Health_404_Score_TextChanged);
            // 
            // Domain1_Box
            // 
            this.Domain1_Box.Controls.Add(this.Node402_Box);
            this.Domain1_Box.Controls.Add(this.Node401_Box);
            this.Domain1_Box.Controls.Add(this.Node201_Box);
            this.Domain1_Box.Controls.Add(this.Node404_Box);
            this.Domain1_Box.Controls.Add(this.Node202_Box);
            this.Domain1_Box.Controls.Add(this.Node403_Box);
            this.Domain1_Box.Controls.Add(this.Node203_Box);
            this.Domain1_Box.Controls.Add(this.Node204_Box);
            this.Domain1_Box.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain1_Box.Location = new System.Drawing.Point(235, 47);
            this.Domain1_Box.Name = "Domain1_Box";
            this.Domain1_Box.Size = new System.Drawing.Size(445, 210);
            this.Domain1_Box.TabIndex = 11;
            this.Domain1_Box.TabStop = false;
            this.Domain1_Box.Text = "Domain 1";
            // 
            // Domain2_Box
            // 
            this.Domain2_Box.Controls.Add(this.Node612_Box);
            this.Domain2_Box.Controls.Add(this.Node611_Box);
            this.Domain2_Box.Controls.Add(this.Node211_Box);
            this.Domain2_Box.Controls.Add(this.Node212_Box);
            this.Domain2_Box.Controls.Add(this.Node412_Box);
            this.Domain2_Box.Controls.Add(this.Node411_Box);
            this.Domain2_Box.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain2_Box.Location = new System.Drawing.Point(697, 47);
            this.Domain2_Box.Name = "Domain2_Box";
            this.Domain2_Box.Size = new System.Drawing.Size(228, 286);
            this.Domain2_Box.TabIndex = 12;
            this.Domain2_Box.TabStop = false;
            this.Domain2_Box.Text = "Domain 2";
            // 
            // Node612_Box
            // 
            this.Node612_Box.Controls.Add(this.Health_612_Score);
            this.Node612_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node612_Box.Location = new System.Drawing.Point(115, 201);
            this.Node612_Box.Name = "Node612_Box";
            this.Node612_Box.Size = new System.Drawing.Size(103, 76);
            this.Node612_Box.TabIndex = 7;
            this.Node612_Box.TabStop = false;
            this.Node612_Box.Text = "NODE 612";
            // 
            // Health_612_Score
            // 
            this.Health_612_Score.AutoSize = true;
            this.Health_612_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_612_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_612_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_612_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_612_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_612_Score.Name = "Health_612_Score";
            this.Health_612_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_612_Score.TabIndex = 11;
            this.Health_612_Score.Text = "N/A";
            this.Health_612_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_612_Score.TextChanged += new System.EventHandler(this.Health_612_Score_TextChanged);
            // 
            // Node611_Box
            // 
            this.Node611_Box.Controls.Add(this.Health_611_Score);
            this.Node611_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node611_Box.Location = new System.Drawing.Point(6, 201);
            this.Node611_Box.Name = "Node611_Box";
            this.Node611_Box.Size = new System.Drawing.Size(103, 76);
            this.Node611_Box.TabIndex = 6;
            this.Node611_Box.TabStop = false;
            this.Node611_Box.Text = "NODE 611";
            // 
            // Health_611_Score
            // 
            this.Health_611_Score.AutoSize = true;
            this.Health_611_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_611_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_611_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_611_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_611_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_611_Score.Name = "Health_611_Score";
            this.Health_611_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_611_Score.TabIndex = 10;
            this.Health_611_Score.Text = "N/A";
            this.Health_611_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_611_Score.TextChanged += new System.EventHandler(this.Health_611_Score_TextChanged);
            // 
            // Node211_Box
            // 
            this.Node211_Box.Controls.Add(this.Health_211_Score);
            this.Node211_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node211_Box.Location = new System.Drawing.Point(6, 37);
            this.Node211_Box.Name = "Node211_Box";
            this.Node211_Box.Size = new System.Drawing.Size(103, 76);
            this.Node211_Box.TabIndex = 2;
            this.Node211_Box.TabStop = false;
            this.Node211_Box.Text = "NODE 211";
            // 
            // Health_211_Score
            // 
            this.Health_211_Score.AutoSize = true;
            this.Health_211_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_211_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_211_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_211_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_211_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_211_Score.Name = "Health_211_Score";
            this.Health_211_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_211_Score.TabIndex = 6;
            this.Health_211_Score.Text = "N/A";
            this.Health_211_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_211_Score.TextChanged += new System.EventHandler(this.Health_211_Score_TextChanged);
            // 
            // Node212_Box
            // 
            this.Node212_Box.Controls.Add(this.Health_212_Score);
            this.Node212_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node212_Box.Location = new System.Drawing.Point(115, 37);
            this.Node212_Box.Name = "Node212_Box";
            this.Node212_Box.Size = new System.Drawing.Size(103, 76);
            this.Node212_Box.TabIndex = 3;
            this.Node212_Box.TabStop = false;
            this.Node212_Box.Text = "NODE 212";
            // 
            // Health_212_Score
            // 
            this.Health_212_Score.AutoSize = true;
            this.Health_212_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_212_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_212_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_212_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_212_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_212_Score.Name = "Health_212_Score";
            this.Health_212_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_212_Score.TabIndex = 7;
            this.Health_212_Score.Text = "N/A";
            this.Health_212_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_212_Score.TextChanged += new System.EventHandler(this.Health_212_Score_TextChanged);
            // 
            // Node412_Box
            // 
            this.Node412_Box.Controls.Add(this.Health_412_Score);
            this.Node412_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node412_Box.Location = new System.Drawing.Point(115, 119);
            this.Node412_Box.Name = "Node412_Box";
            this.Node412_Box.Size = new System.Drawing.Size(103, 76);
            this.Node412_Box.TabIndex = 5;
            this.Node412_Box.TabStop = false;
            this.Node412_Box.Text = "NODE 412";
            // 
            // Health_412_Score
            // 
            this.Health_412_Score.AutoSize = true;
            this.Health_412_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_412_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_412_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_412_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_412_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_412_Score.Name = "Health_412_Score";
            this.Health_412_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_412_Score.TabIndex = 9;
            this.Health_412_Score.Text = "N/A";
            this.Health_412_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_412_Score.TextChanged += new System.EventHandler(this.Health_412_Score_TextChanged);
            // 
            // Node411_Box
            // 
            this.Node411_Box.Controls.Add(this.Health_411_Score);
            this.Node411_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node411_Box.Location = new System.Drawing.Point(6, 119);
            this.Node411_Box.Name = "Node411_Box";
            this.Node411_Box.Size = new System.Drawing.Size(103, 76);
            this.Node411_Box.TabIndex = 4;
            this.Node411_Box.TabStop = false;
            this.Node411_Box.Text = "NODE 411";
            // 
            // Health_411_Score
            // 
            this.Health_411_Score.AutoSize = true;
            this.Health_411_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_411_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_411_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_411_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_411_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_411_Score.Name = "Health_411_Score";
            this.Health_411_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_411_Score.TabIndex = 8;
            this.Health_411_Score.Text = "N/A";
            this.Health_411_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_411_Score.TextChanged += new System.EventHandler(this.Health_411_Score_TextChanged);
            // 
            // Domain3_Box
            // 
            this.Domain3_Box.Controls.Add(this.Node622_Box);
            this.Domain3_Box.Controls.Add(this.Node621_Box);
            this.Domain3_Box.Controls.Add(this.Node422_Box);
            this.Domain3_Box.Controls.Add(this.Node421_Box);
            this.Domain3_Box.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain3_Box.Location = new System.Drawing.Point(235, 263);
            this.Domain3_Box.Name = "Domain3_Box";
            this.Domain3_Box.Size = new System.Drawing.Size(225, 208);
            this.Domain3_Box.TabIndex = 13;
            this.Domain3_Box.TabStop = false;
            this.Domain3_Box.Text = "Domain 3";
            // 
            // Node622_Box
            // 
            this.Node622_Box.Controls.Add(this.Health_622_Score);
            this.Node622_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node622_Box.Location = new System.Drawing.Point(115, 119);
            this.Node622_Box.Name = "Node622_Box";
            this.Node622_Box.Size = new System.Drawing.Size(103, 76);
            this.Node622_Box.TabIndex = 7;
            this.Node622_Box.TabStop = false;
            this.Node622_Box.Text = "NODE 622";
            // 
            // Health_622_Score
            // 
            this.Health_622_Score.AutoSize = true;
            this.Health_622_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_622_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_622_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_622_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_622_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_622_Score.Name = "Health_622_Score";
            this.Health_622_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_622_Score.TabIndex = 13;
            this.Health_622_Score.Text = "N/A";
            this.Health_622_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_622_Score.TextChanged += new System.EventHandler(this.Health_622_Score_TextChanged);
            // 
            // Node621_Box
            // 
            this.Node621_Box.Controls.Add(this.Health_621_Score);
            this.Node621_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node621_Box.Location = new System.Drawing.Point(6, 119);
            this.Node621_Box.Name = "Node621_Box";
            this.Node621_Box.Size = new System.Drawing.Size(103, 76);
            this.Node621_Box.TabIndex = 6;
            this.Node621_Box.TabStop = false;
            this.Node621_Box.Text = "NODE 621";
            // 
            // Health_621_Score
            // 
            this.Health_621_Score.AutoSize = true;
            this.Health_621_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_621_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_621_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_621_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_621_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_621_Score.Name = "Health_621_Score";
            this.Health_621_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_621_Score.TabIndex = 12;
            this.Health_621_Score.Text = "N/A";
            this.Health_621_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_621_Score.TextChanged += new System.EventHandler(this.Health_621_Score_TextChanged);
            // 
            // Node422_Box
            // 
            this.Node422_Box.Controls.Add(this.Health_422_Score);
            this.Node422_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node422_Box.Location = new System.Drawing.Point(116, 37);
            this.Node422_Box.Name = "Node422_Box";
            this.Node422_Box.Size = new System.Drawing.Size(103, 76);
            this.Node422_Box.TabIndex = 5;
            this.Node422_Box.TabStop = false;
            this.Node422_Box.Text = "NODE 422";
            // 
            // Health_422_Score
            // 
            this.Health_422_Score.AutoSize = true;
            this.Health_422_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_422_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_422_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_422_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_422_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_422_Score.Name = "Health_422_Score";
            this.Health_422_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_422_Score.TabIndex = 11;
            this.Health_422_Score.Text = "N/A";
            this.Health_422_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_422_Score.TextChanged += new System.EventHandler(this.Health_422_Score_TextChanged);
            // 
            // Node421_Box
            // 
            this.Node421_Box.Controls.Add(this.Health_421_Score);
            this.Node421_Box.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node421_Box.Location = new System.Drawing.Point(6, 37);
            this.Node421_Box.Name = "Node421_Box";
            this.Node421_Box.Size = new System.Drawing.Size(103, 76);
            this.Node421_Box.TabIndex = 4;
            this.Node421_Box.TabStop = false;
            this.Node421_Box.Text = "NODE 421";
            // 
            // Health_421_Score
            // 
            this.Health_421_Score.AutoSize = true;
            this.Health_421_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_421_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_421_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_421_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_421_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_421_Score.Name = "Health_421_Score";
            this.Health_421_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_421_Score.TabIndex = 10;
            this.Health_421_Score.Text = "N/A";
            this.Health_421_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_421_Score.TextChanged += new System.EventHandler(this.Health_421_Score_TextChanged);
            // 
            // Menu_Strip_1
            // 
            this.Menu_Strip_1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dEBUGToolStripMenuItem});
            this.Menu_Strip_1.Location = new System.Drawing.Point(0, 0);
            this.Menu_Strip_1.Name = "Menu_Strip_1";
            this.Menu_Strip_1.Size = new System.Drawing.Size(960, 24);
            this.Menu_Strip_1.TabIndex = 14;
            this.Menu_Strip_1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.setingToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.loginToolStripMenuItem.Text = "Login";
            this.loginToolStripMenuItem.Click += new System.EventHandler(this.LoginToolStripMenuItem_Click);
            // 
            // setingToolStripMenuItem
            // 
            this.setingToolStripMenuItem.Name = "setingToolStripMenuItem";
            this.setingToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.setingToolStripMenuItem.Text = "Seting";
            this.setingToolStripMenuItem.Click += new System.EventHandler(this.SetingToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // dEBUGToolStripMenuItem
            // 
            this.dEBUGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tESTLineToolStripMenuItem});
            this.dEBUGToolStripMenuItem.Name = "dEBUGToolStripMenuItem";
            this.dEBUGToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.dEBUGToolStripMenuItem.Text = "DEBUG";
            // 
            // tESTLineToolStripMenuItem
            // 
            this.tESTLineToolStripMenuItem.Name = "tESTLineToolStripMenuItem";
            this.tESTLineToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.tESTLineToolStripMenuItem.Text = "TEST_Line";
            // 
            // Fabic_Box
            // 
            this.Fabic_Box.Controls.Add(this.groupBox1);
            this.Fabic_Box.Controls.Add(this.Health_POD1_score);
            this.Fabic_Box.Controls.Add(this.Node502_Box);
            this.Fabic_Box.Controls.Add(this.Node501_Box);
            this.Fabic_Box.Controls.Add(this.Node302_Box);
            this.Fabic_Box.Controls.Add(this.Node301_Box);
            this.Fabic_Box.Controls.Add(this.Node102_Box);
            this.Fabic_Box.Controls.Add(this.Node101_Box);
            this.Fabic_Box.Controls.Add(this.Domain2_Box);
            this.Fabic_Box.Controls.Add(this.Domain3_Box);
            this.Fabic_Box.Controls.Add(this.Domain1_Box);
            this.Fabic_Box.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fabic_Box.Location = new System.Drawing.Point(12, 27);
            this.Fabic_Box.Name = "Fabic_Box";
            this.Fabic_Box.Size = new System.Drawing.Size(938, 483);
            this.Fabic_Box.TabIndex = 15;
            this.Fabic_Box.TabStop = false;
            this.Fabic_Box.Text = "[FABIC] POD-1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(466, 263);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(225, 208);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Domain 4";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Health_632_Score);
            this.groupBox2.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(115, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(103, 76);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "NODE 632";
            // 
            // Health_632_Score
            // 
            this.Health_632_Score.AutoSize = true;
            this.Health_632_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_632_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_632_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_632_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_632_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_632_Score.Name = "Health_632_Score";
            this.Health_632_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_632_Score.TabIndex = 13;
            this.Health_632_Score.Text = "N/A";
            this.Health_632_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_632_Score.TextChanged += new System.EventHandler(this.Health_632_Score_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Health_631_Score);
            this.groupBox3.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 119);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(103, 76);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "NODE 631";
            // 
            // Health_631_Score
            // 
            this.Health_631_Score.AutoSize = true;
            this.Health_631_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_631_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_631_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_631_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_631_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_631_Score.Name = "Health_631_Score";
            this.Health_631_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_631_Score.TabIndex = 12;
            this.Health_631_Score.Text = "N/A";
            this.Health_631_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_631_Score.TextChanged += new System.EventHandler(this.Health_631_Score_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Health_232_Score);
            this.groupBox4.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(116, 37);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(103, 76);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "NODE 232";
            // 
            // Health_232_Score
            // 
            this.Health_232_Score.AutoSize = true;
            this.Health_232_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_232_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_232_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_232_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_232_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_232_Score.Name = "Health_232_Score";
            this.Health_232_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_232_Score.TabIndex = 11;
            this.Health_232_Score.Text = "N/A";
            this.Health_232_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_232_Score.TextChanged += new System.EventHandler(this.Health_232_Score_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Health_231_Score);
            this.groupBox5.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(6, 37);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(103, 76);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "NODE 231";
            // 
            // Health_231_Score
            // 
            this.Health_231_Score.AutoSize = true;
            this.Health_231_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_231_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_231_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_231_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_231_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_231_Score.Name = "Health_231_Score";
            this.Health_231_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_231_Score.TabIndex = 10;
            this.Health_231_Score.Text = "N/A";
            this.Health_231_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_231_Score.TextChanged += new System.EventHandler(this.Health_231_Score_TextChanged);
            // 
            // Health_POD1_score
            // 
            this.Health_POD1_score.BackColor = System.Drawing.SystemColors.Control;
            this.Health_POD1_score.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Health_POD1_score.Cursor = System.Windows.Forms.Cursors.Default;
            this.Health_POD1_score.Font = new System.Drawing.Font("Comic Sans MS", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_POD1_score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_POD1_score.Location = new System.Drawing.Point(0, 39);
            this.Health_POD1_score.Name = "Health_POD1_score";
            this.Health_POD1_score.ReadOnly = true;
            this.Health_POD1_score.Size = new System.Drawing.Size(229, 112);
            this.Health_POD1_score.TabIndex = 14;
            this.Health_POD1_score.TabStop = false;
            this.Health_POD1_score.Text = "N/A";
            this.Health_POD1_score.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Info_tooltip.SetToolTip(this.Health_POD1_score, "POD1");
            this.Health_POD1_score.TextChanged += new System.EventHandler(this.Health_POD1_score_TextChanged);
            // 
            // Node502_Box
            // 
            this.Node502_Box.Controls.Add(this.Health_502_Score);
            this.Node502_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node502_Box.Location = new System.Drawing.Point(126, 330);
            this.Node502_Box.Name = "Node502_Box";
            this.Node502_Box.Size = new System.Drawing.Size(103, 76);
            this.Node502_Box.TabIndex = 4;
            this.Node502_Box.TabStop = false;
            this.Node502_Box.Text = "Spine SRB 502";
            // 
            // Health_502_Score
            // 
            this.Health_502_Score.AutoSize = true;
            this.Health_502_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_502_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_502_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_502_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_502_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_502_Score.Name = "Health_502_Score";
            this.Health_502_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_502_Score.TabIndex = 0;
            this.Health_502_Score.Text = "N/A";
            this.Health_502_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_502_Score.TextChanged += new System.EventHandler(this.Health_502_Score_TextChanged);
            // 
            // Node501_Box
            // 
            this.Node501_Box.Controls.Add(this.Health_501_Score);
            this.Node501_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node501_Box.Location = new System.Drawing.Point(6, 330);
            this.Node501_Box.Name = "Node501_Box";
            this.Node501_Box.Size = new System.Drawing.Size(103, 76);
            this.Node501_Box.TabIndex = 4;
            this.Node501_Box.TabStop = false;
            this.Node501_Box.Text = "Spine SRB 501";
            // 
            // Health_501_Score
            // 
            this.Health_501_Score.AutoSize = true;
            this.Health_501_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_501_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_501_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_501_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_501_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_501_Score.Name = "Health_501_Score";
            this.Health_501_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_501_Score.TabIndex = 0;
            this.Health_501_Score.Text = "N/A";
            this.Health_501_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_501_Score.TextChanged += new System.EventHandler(this.Health_501_Score_TextChanged);
            // 
            // Node302_Box
            // 
            this.Node302_Box.Controls.Add(this.Health_302_Score);
            this.Node302_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node302_Box.Location = new System.Drawing.Point(126, 248);
            this.Node302_Box.Name = "Node302_Box";
            this.Node302_Box.Size = new System.Drawing.Size(103, 76);
            this.Node302_Box.TabIndex = 4;
            this.Node302_Box.TabStop = false;
            this.Node302_Box.Text = "Spine BTT 302";
            // 
            // Health_302_Score
            // 
            this.Health_302_Score.AutoSize = true;
            this.Health_302_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_302_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_302_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_302_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_302_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_302_Score.Name = "Health_302_Score";
            this.Health_302_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_302_Score.TabIndex = 0;
            this.Health_302_Score.Text = "N/A";
            this.Health_302_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_302_Score.TextChanged += new System.EventHandler(this.Health_302_Score_TextChanged);
            // 
            // Node301_Box
            // 
            this.Node301_Box.Controls.Add(this.Health_301_Score);
            this.Node301_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node301_Box.Location = new System.Drawing.Point(6, 248);
            this.Node301_Box.Name = "Node301_Box";
            this.Node301_Box.Size = new System.Drawing.Size(103, 76);
            this.Node301_Box.TabIndex = 4;
            this.Node301_Box.TabStop = false;
            this.Node301_Box.Text = "Spine BTT 301";
            // 
            // Health_301_Score
            // 
            this.Health_301_Score.AutoSize = true;
            this.Health_301_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_301_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_301_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_301_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_301_Score.Location = new System.Drawing.Point(6, 22);
            this.Health_301_Score.Name = "Health_301_Score";
            this.Health_301_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_301_Score.TabIndex = 0;
            this.Health_301_Score.Text = "N/A";
            this.Health_301_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_301_Score.TextChanged += new System.EventHandler(this.Health_301_Score_TextChanged);
            // 
            // Node102_Box
            // 
            this.Node102_Box.Controls.Add(this.Health_102_Score);
            this.Node102_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node102_Box.Location = new System.Drawing.Point(126, 166);
            this.Node102_Box.Name = "Node102_Box";
            this.Node102_Box.Size = new System.Drawing.Size(103, 76);
            this.Node102_Box.TabIndex = 4;
            this.Node102_Box.TabStop = false;
            this.Node102_Box.Text = "Spine TST 102";
            // 
            // Health_102_Score
            // 
            this.Health_102_Score.AutoSize = true;
            this.Health_102_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_102_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_102_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_102_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_102_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_102_Score.Name = "Health_102_Score";
            this.Health_102_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_102_Score.TabIndex = 0;
            this.Health_102_Score.Text = "N/A";
            this.Health_102_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_102_Score.TextChanged += new System.EventHandler(this.Health_102_Score_TextChanged);
            // 
            // Node101_Box
            // 
            this.Node101_Box.Controls.Add(this.Health_101_Score);
            this.Node101_Box.Font = new System.Drawing.Font("Comic Sans MS", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Node101_Box.Location = new System.Drawing.Point(6, 166);
            this.Node101_Box.Name = "Node101_Box";
            this.Node101_Box.Size = new System.Drawing.Size(103, 76);
            this.Node101_Box.TabIndex = 3;
            this.Node101_Box.TabStop = false;
            this.Node101_Box.Text = "Spine TST 101";
            // 
            // Health_101_Score
            // 
            this.Health_101_Score.AutoSize = true;
            this.Health_101_Score.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Health_101_Score.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Health_101_Score.ForeColor = System.Drawing.Color.Yellow;
            this.Health_101_Score.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_101_Score.Location = new System.Drawing.Point(12, 22);
            this.Health_101_Score.Name = "Health_101_Score";
            this.Health_101_Score.Size = new System.Drawing.Size(85, 45);
            this.Health_101_Score.TabIndex = 0;
            this.Health_101_Score.Text = "N/A";
            this.Health_101_Score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Health_101_Score.TextChanged += new System.EventHandler(this.Health_101_Score_TextChanged);
            // 
            // AAA_Refresh
            // 
            this.AAA_Refresh.Interval = 180000D;
            this.AAA_Refresh.SynchronizingObject = this;
            this.AAA_Refresh.Elapsed += new System.Timers.ElapsedEventHandler(this.AAA_Refresh_Elapsed);
            // 
            // BGW_POD1
            // 
            this.BGW_POD1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_POD1_DoWork);
            // 
            // BGW_Spine_101
            // 
            this.BGW_Spine_101.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_101_DoWork);
            // 
            // Timer_SPINE_BGW
            // 
            this.Timer_SPINE_BGW.Interval = 5000;
            this.Timer_SPINE_BGW.Tick += new System.EventHandler(this.Timer_SPINE_BGW_Tick);
            // 
            // BGW_Spine_102
            // 
            this.BGW_Spine_102.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_102_DoWork);
            // 
            // BGW_Spine_301
            // 
            this.BGW_Spine_301.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_301_DoWork);
            // 
            // BGW_Spine_302
            // 
            this.BGW_Spine_302.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_302_DoWork);
            // 
            // BGW_Spine_501
            // 
            this.BGW_Spine_501.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_501_DoWork);
            // 
            // BGW_Spine_502
            // 
            this.BGW_Spine_502.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Spine_502_DoWork);
            // 
            // BGW_Node_201
            // 
            this.BGW_Node_201.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_201_DoWork);
            // 
            // BGW_Node_202
            // 
            this.BGW_Node_202.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_202_DoWork);
            // 
            // BGW_Node_203
            // 
            this.BGW_Node_203.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_203_DoWork);
            // 
            // BGW_Node_204
            // 
            this.BGW_Node_204.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_204_DoWork);
            // 
            // BGW_Node_401
            // 
            this.BGW_Node_401.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_401_DoWork);
            // 
            // BGW_Node_402
            // 
            this.BGW_Node_402.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_402_DoWork);
            // 
            // BGW_Node_403
            // 
            this.BGW_Node_403.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_403_DoWork);
            // 
            // BGW_Node_404
            // 
            this.BGW_Node_404.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_404_DoWork);
            // 
            // BGW_Node_211
            // 
            this.BGW_Node_211.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_211_DoWork);
            // 
            // BGW_Node_212
            // 
            this.BGW_Node_212.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_212_DoWork);
            // 
            // BGW_Node_411
            // 
            this.BGW_Node_411.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_411_DoWork);
            // 
            // BGW_Node_412
            // 
            this.BGW_Node_412.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_412_DoWork);
            // 
            // BGW_Node_611
            // 
            this.BGW_Node_611.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_611_DoWork);
            // 
            // BGW_Node_612
            // 
            this.BGW_Node_612.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_612_DoWork);
            // 
            // BGW_Node_221
            // 
            this.BGW_Node_221.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_221_DoWork);
            // 
            // BGW_Node_222
            // 
            this.BGW_Node_222.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_222_DoWork);
            // 
            // BGW_Node_421
            // 
            this.BGW_Node_421.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_421_DoWork);
            // 
            // BGW_Node_422
            // 
            this.BGW_Node_422.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_422_DoWork);
            // 
            // BGW_Node_621
            // 
            this.BGW_Node_621.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_621_DoWork);
            // 
            // BGW_Node_622
            // 
            this.BGW_Node_622.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_622_DoWork);
            // 
            // Timer_DOM1_BGW
            // 
            this.Timer_DOM1_BGW.Interval = 5000;
            this.Timer_DOM1_BGW.Tick += new System.EventHandler(this.Timer_DOM1_BGW_Tick);
            // 
            // Timer_DOM2_BGW
            // 
            this.Timer_DOM2_BGW.Interval = 5000;
            this.Timer_DOM2_BGW.Tick += new System.EventHandler(this.Timer_DOM2_BGW_Tick);
            // 
            // Timer_DOM3_BGW
            // 
            this.Timer_DOM3_BGW.Interval = 5000;
            this.Timer_DOM3_BGW.Tick += new System.EventHandler(this.Timer_DOM3_BGW_Tick);
            // 
            // BGW_Node_231
            // 
            this.BGW_Node_231.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_231_DoWork);
            // 
            // BGW_Node_232
            // 
            this.BGW_Node_232.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_232_DoWork);
            // 
            // BGW_Node_631
            // 
            this.BGW_Node_631.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_631_DoWork);
            // 
            // BGW_Node_632
            // 
            this.BGW_Node_632.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Node_632_DoWork);
            // 
            // Timer_DOM4_BGW
            // 
            this.Timer_DOM4_BGW.Interval = 5000;
            this.Timer_DOM4_BGW.Tick += new System.EventHandler(this.Timer_DOM4_BGW_Tick);
            // 
            // MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(960, 540);
            this.Controls.Add(this.Fabic_Box);
            this.Controls.Add(this.Status_Strip_1);
            this.Controls.Add(this.Menu_Strip_1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACI-INTERFACE-MONITOR V2";
            this.Status_Strip_1.ResumeLayout(false);
            this.Status_Strip_1.PerformLayout();
            this.Node201_Box.ResumeLayout(false);
            this.Node201_Box.PerformLayout();
            this.Node202_Box.ResumeLayout(false);
            this.Node202_Box.PerformLayout();
            this.Node401_Box.ResumeLayout(false);
            this.Node401_Box.PerformLayout();
            this.Node402_Box.ResumeLayout(false);
            this.Node402_Box.PerformLayout();
            this.Node203_Box.ResumeLayout(false);
            this.Node203_Box.PerformLayout();
            this.Node204_Box.ResumeLayout(false);
            this.Node204_Box.PerformLayout();
            this.Node403_Box.ResumeLayout(false);
            this.Node403_Box.PerformLayout();
            this.Node404_Box.ResumeLayout(false);
            this.Node404_Box.PerformLayout();
            this.Domain1_Box.ResumeLayout(false);
            this.Domain2_Box.ResumeLayout(false);
            this.Node612_Box.ResumeLayout(false);
            this.Node612_Box.PerformLayout();
            this.Node611_Box.ResumeLayout(false);
            this.Node611_Box.PerformLayout();
            this.Node211_Box.ResumeLayout(false);
            this.Node211_Box.PerformLayout();
            this.Node212_Box.ResumeLayout(false);
            this.Node212_Box.PerformLayout();
            this.Node412_Box.ResumeLayout(false);
            this.Node412_Box.PerformLayout();
            this.Node411_Box.ResumeLayout(false);
            this.Node411_Box.PerformLayout();
            this.Domain3_Box.ResumeLayout(false);
            this.Node622_Box.ResumeLayout(false);
            this.Node622_Box.PerformLayout();
            this.Node621_Box.ResumeLayout(false);
            this.Node621_Box.PerformLayout();
            this.Node422_Box.ResumeLayout(false);
            this.Node422_Box.PerformLayout();
            this.Node421_Box.ResumeLayout(false);
            this.Node421_Box.PerformLayout();
            this.Menu_Strip_1.ResumeLayout(false);
            this.Menu_Strip_1.PerformLayout();
            this.Fabic_Box.ResumeLayout(false);
            this.Fabic_Box.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Node502_Box.ResumeLayout(false);
            this.Node502_Box.PerformLayout();
            this.Node501_Box.ResumeLayout(false);
            this.Node501_Box.PerformLayout();
            this.Node302_Box.ResumeLayout(false);
            this.Node302_Box.PerformLayout();
            this.Node301_Box.ResumeLayout(false);
            this.Node301_Box.PerformLayout();
            this.Node102_Box.ResumeLayout(false);
            this.Node102_Box.PerformLayout();
            this.Node101_Box.ResumeLayout(false);
            this.Node101_Box.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AAA_Refresh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip Status_Strip_1;
        private System.Windows.Forms.GroupBox Node201_Box;
        private System.Windows.Forms.GroupBox Node202_Box;
        private System.Windows.Forms.GroupBox Node401_Box;
        private System.Windows.Forms.GroupBox Node402_Box;
        private System.Windows.Forms.GroupBox Node203_Box;
        private System.Windows.Forms.GroupBox Node204_Box;
        private System.Windows.Forms.GroupBox Node403_Box;
        private System.Windows.Forms.GroupBox Node404_Box;
        private System.Windows.Forms.GroupBox Domain1_Box;
        private System.Windows.Forms.GroupBox Domain2_Box;
        private System.Windows.Forms.GroupBox Node612_Box;
        private System.Windows.Forms.GroupBox Node611_Box;
        private System.Windows.Forms.GroupBox Node211_Box;
        private System.Windows.Forms.GroupBox Node212_Box;
        private System.Windows.Forms.GroupBox Node412_Box;
        private System.Windows.Forms.GroupBox Node411_Box;
        private System.Windows.Forms.GroupBox Domain3_Box;
        private System.Windows.Forms.GroupBox Node622_Box;
        private System.Windows.Forms.GroupBox Node621_Box;
        private System.Windows.Forms.GroupBox Node422_Box;
        private System.Windows.Forms.GroupBox Node421_Box;
        private System.Windows.Forms.ToolStripStatusLabel Status_bar;
        private System.Windows.Forms.MenuStrip Menu_Strip_1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.Label Health_201_Score;
        private System.Windows.Forms.Label Health_202_Score;
        private System.Windows.Forms.Label Health_401_Score;
        private System.Windows.Forms.Label Health_402_Score;
        private System.Windows.Forms.Label Health_203_Score;
        private System.Windows.Forms.Label Health_204_Score;
        private System.Windows.Forms.Label Health_403_Score;
        private System.Windows.Forms.Label Health_404_Score;
        private System.Windows.Forms.Label Health_612_Score;
        private System.Windows.Forms.Label Health_611_Score;
        private System.Windows.Forms.Label Health_211_Score;
        private System.Windows.Forms.Label Health_212_Score;
        private System.Windows.Forms.Label Health_412_Score;
        private System.Windows.Forms.Label Health_411_Score;
        private System.Windows.Forms.Label Health_622_Score;
        private System.Windows.Forms.Label Health_621_Score;
        private System.Windows.Forms.Label Health_422_Score;
        private System.Windows.Forms.Label Health_421_Score;
        private System.Windows.Forms.GroupBox Fabic_Box;
        private System.Windows.Forms.GroupBox Node502_Box;
        private System.Windows.Forms.Label Health_502_Score;
        private System.Windows.Forms.GroupBox Node501_Box;
        private System.Windows.Forms.Label Health_501_Score;
        private System.Windows.Forms.GroupBox Node302_Box;
        private System.Windows.Forms.Label Health_302_Score;
        private System.Windows.Forms.GroupBox Node301_Box;
        private System.Windows.Forms.Label Health_301_Score;
        private System.Windows.Forms.GroupBox Node102_Box;
        private System.Windows.Forms.Label Health_102_Score;
        private System.Windows.Forms.GroupBox Node101_Box;
        private System.Windows.Forms.Label Health_101_Score;
        private System.Timers.Timer AAA_Refresh;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker BGW_POD1;
        private System.Windows.Forms.TextBox Health_POD1_score;
        private System.ComponentModel.BackgroundWorker BGW_Spine_101;
        private System.Windows.Forms.Timer Timer_SPINE_BGW;
        private System.ComponentModel.BackgroundWorker BGW_Spine_102;
        private System.ComponentModel.BackgroundWorker BGW_Spine_301;
        private System.ComponentModel.BackgroundWorker BGW_Spine_302;
        private System.ComponentModel.BackgroundWorker BGW_Spine_501;
        private System.ComponentModel.BackgroundWorker BGW_Spine_502;
        private System.ComponentModel.BackgroundWorker BGW_Node_201;
        private System.ComponentModel.BackgroundWorker BGW_Node_202;
        private System.ComponentModel.BackgroundWorker BGW_Node_203;
        private System.ComponentModel.BackgroundWorker BGW_Node_204;
        private System.ComponentModel.BackgroundWorker BGW_Node_401;
        private System.ComponentModel.BackgroundWorker BGW_Node_402;
        private System.ComponentModel.BackgroundWorker BGW_Node_403;
        private System.ComponentModel.BackgroundWorker BGW_Node_404;
        private System.ComponentModel.BackgroundWorker BGW_Node_211;
        private System.ComponentModel.BackgroundWorker BGW_Node_212;
        private System.ComponentModel.BackgroundWorker BGW_Node_411;
        private System.ComponentModel.BackgroundWorker BGW_Node_412;
        private System.ComponentModel.BackgroundWorker BGW_Node_611;
        private System.ComponentModel.BackgroundWorker BGW_Node_612;
        private System.ComponentModel.BackgroundWorker BGW_Node_221;
        private System.ComponentModel.BackgroundWorker BGW_Node_222;
        private System.ComponentModel.BackgroundWorker BGW_Node_421;
        private System.ComponentModel.BackgroundWorker BGW_Node_422;
        private System.ComponentModel.BackgroundWorker BGW_Node_621;
        private System.ComponentModel.BackgroundWorker BGW_Node_622;
        private System.Windows.Forms.Timer Timer_DOM1_BGW;
        private System.Windows.Forms.Timer Timer_DOM2_BGW;
        private System.Windows.Forms.Timer Timer_DOM3_BGW;
        private System.Windows.Forms.ToolStripMenuItem setingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dEBUGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTLineToolStripMenuItem;
        private System.Windows.Forms.ToolTip Info_tooltip;
        private System.ComponentModel.BackgroundWorker BGW_Node_231;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label Health_632_Score;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Health_631_Score;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label Health_232_Score;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label Health_231_Score;
        private System.ComponentModel.BackgroundWorker BGW_Node_232;
        private System.ComponentModel.BackgroundWorker BGW_Node_631;
        private System.ComponentModel.BackgroundWorker BGW_Node_632;
        private System.Windows.Forms.Timer Timer_DOM4_BGW;
    }
}

